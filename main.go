package main

import (
	"os"

	"bitbucket.org/JacobFerrier/bloomfilter"
	"bitbucket.org/JacobFerrier/racco/functions"
	"bitbucket.org/JacobFerrier/racco/testing"
)

func main() {
	var mainDir = functions.Inter(os.Getwd())[0].(string)

	// build necessary directories
	// TODO:

	// creates randomly generated datasets used for profile experimentation
	testing.CreateRandomDatasets((mainDir + "/test_datasets/"), 100, 20000, 7)

	// profiles datasets
	//reduction.ProfileDatasets((mainDir + "/test_profiles/reduction/"), (mainDir + "/test_datasets/"))
	//indexing.ProfileDatasets((mainDir + "/test_profiles/indexing/"), (mainDir + "/test_datasets/"))
	bloomfilter.ProfileDatasets((mainDir + "/test_profiles/bloomfilter/"), (mainDir + "/test_datasets/"), 0.03)

	// testing filter matching
	bloomfilter.FindFilterMatch((mainDir + "/test_profiles/bloomfilter/0000.profile"), (mainDir + "/test_datasets/"))
}
