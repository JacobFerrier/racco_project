package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

var datasetsDir string = "/users/jferrier/racco/datasets"

type Gene struct {
	db, db_object_id, db_object_symbol, go_id, db_reference, taxon, date string
}

type Dataset struct {
	name, source, species string
	gene_list []Gene
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func updateDatasetWithGenes(d *Dataset) {
	f, err := ioutil.ReadFile(datasetsDir + "/" +  d.source + "/" + d.name)
	check(err)

	split := strings.Split(string(f), "\n")

	var l []string
	for i := 0; i < len(split); i++ {
		if (!strings.HasPrefix(split[i], "!")) {
			l = strings.Split(split[i], "\t")
			d.gene_list = append(d.gene_list, Gene{db: l[0], db_object_id: l[1], db_object_symbol: l[2], go_id: l[4], db_reference: l[5], taxon: l[12], date: l[13]})
		}
	}
}

func main() {
	homo_sapien := Dataset{name: "goa_human.gaf", source: "gene_ontology_consortium", species: "homo sapien"}

	fmt.Println(homo_sapien)
	updateDatasetWithGenes(&homo_sapien)

	fmt.Println(homo_sapien.gene_list[23214])
}
